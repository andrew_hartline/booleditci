<?php
require_once("includes.php");

/**
 * An Atom is an elementary Formula, i.e. it cannot be subdivided.
 * Syntactically it is a single letter---here I am requiring that
 * it occur in the Roman alphabet but this could easily be changed.
 */
class Atom extends SimpleFormula implements SimplePrint
{
    private $myLetter;

    /**
     * Constructor
     * @param String $c Basically a char, but this is php and I have to use a string
     *                  with enforced 1-char limit. Must be alphabetical.
     */
    public function __construct($c)
    {
        if (preg_match("/^[a-zA-Z]$/", $c))
            $this->myLetter = $c;
        else throw new SyntaxException("bad input to Atom constructor: $c");
    }

    public function toString()
    {
        return $this->myLetter;
    }

    public function getChar()
    {
        return $this->myLetter;
    }

    /**
     * The terminus of every recursive uniform substitution call.
     * @param  Formula $f The formula that is going to be substituted for $a
     * @param  Atom    $a The atom that $f is supposed to sub in for.
     * @return Formula    $f, if I am $a; myself otherwise.
     */
    public function uSub(Formula $f, Atom $a)
    {
        if ($this->equals($a))
            return $f;
        else
            return $this;
    }

    /**
     * The terminus of every recursive uSubbableConstrained call.
     * @param  Formula $f      Formula that might be subbed in for me, if I am not
     *                         already getting subbed by something else;
     * @param  [type]  $subMap Map of the substitutions that have already been made.
     * @return bool            Success message if all is well so far in the bigger
     *                         recursive function.
     */
    public function uSubbableConstrained(Formula $f, &$subMap)
    {
        $output = false;
        $myLetter = $this->myLetter;
        if ($f == NULL && array_key_exists($myLetter, $subMap))
            $output = $submap[$myLetter]->equals($f);
        else
        {
            $output = true;
            $subMap[$myLetter] = $f;
        }

        return $output;
    }

    /**
     *   Trivially true of every Atom... needs to be here so that recursive trips
     *   into a formula bottom out somewhere.
     *   @param Formula f  Formula object
     */
    public function uSubbable(Formula $f)
    {
        return true;
    }

    public function getFormulae()
    {
        return null;
    }
}
?>