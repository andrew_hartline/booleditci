<?php

/**
 * A SimpleFormula is a Formula with no inner structure, in other words either
 * a letter or the ints 1 or 0. SimpleFormulae are where my rule checking
 * algorithms bottom out.
 */
abstract class SimpleFormula extends Formula
{
    public function __construct()
    {
        $this->myConnective = null;
        $this->myFormulae = new SplFixedArray(1);
    }
}

?>