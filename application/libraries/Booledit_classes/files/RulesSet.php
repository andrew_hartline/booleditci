<?php

/**
 * A RulesSet represents all the rules that are available within a given
 * version of Boolean algebra. It isn't really a set because I don't
 * enforce uniqueness.
 */
class RulesSet
{
    private $myRules;

    public function __construct()
    {
        $myRules = array();
    }

    public function addRule($name, $rule)
    {
        if (!is_string($name) || !is_string($rule))
            throw new InvalidArgumentException("addRule wants strings, " .
                                               "you gave it " .
                                               gettype($rule));
        else
        {
            $name = strtoupper(strip_tags($name));
            $split = explode("=", $rule);
            $this->myRules[$name] = new EquivalenceRule($name,
                                                        $split[0],
                                                        $split[1]);
        }
    }

    public function getRule($name)
    {
        $output = null;

        if (!is_string($name))
            throw new InvalidArgumentException("getRule wants strings, " .
                                               "you gave it " .
                                               gettype($name));
        $name = strtoupper(strip_tags($name));

        if (strtolower($name) == "null") return null;

        if (array_key_exists($name, $this->myRules))
            $output = $this->myRules[$name];

        return $output;
    }

    public function toStringArray()
    {
        $output = array();
        foreach ($this->myRules as $rule)
            $output[] = $rule->toString();

        return $output;
    }
}

?>
