<?php

require_once("includes.php");

/**
 * A BoolParser handles the conversion of strings entered by the programmer
 * or user to Formula objects.
 */
class BoolParser
{
/**
 * The core functionality of this class. Does syntax checking too.
 * @param  string $myInfix A string, should be a Boolan formula in infix notation.
 * @return Formula         Fully constructed Formula object.
 */
    public static function formulaFactory($myInfix)
    {
        if (!is_string($myInfix))
            throw new Exception("formulaFactory needs a string as input");
        return BoolParser::postfixFormula(BoolParser::DSY($myInfix));
    }

/**
 * Implementation of Dijkstra's Shunting Yard algorithm for converting strings in
 * infix to strings in postfix.
 * @param string $infix A string representing a Boolean formula in infix notation.
 * @return string       A string representing the same Boolean formula in postfix
 *                      notation.
 */
    public static function DSY($infix)
    {
        $debug = false;

        if (!is_string($infix))
            throw new Exception("DSY needs a string as input");

        $output = "";
        $operatorStack = array();
        $weights = array("+" => 1,
                         "*" => 2,
                         "'" => 3);

        for ($i = 0; $i < strlen($infix); $i++)
        {
            $c = $infix[$i];
            if (BoolParser::isOperand($c))
            {
                $output = $output . $c;
                if($debug) echo "added $c to output: $output\n";
            }
            else if (BoolParser::isOperator($c))
            {
                while ( count($operatorStack) > 0 &&
                        end($operatorStack) != "(" &&
                        $weights[$c] <= $weights[end($operatorStack)] )
                {
                    $output = $output . array_pop($operatorStack);
                    if($debug) echo "c is $c; output now $output\n";
                }

                array_push($operatorStack, $c);
                if($debug) echo "added $c to opStack\n";

            }
            else if ($c == "(")
                array_push($operatorStack, $c);
            else if ($c == ")") // Whatever is inside parentheses is top priority
            {
                while (count($operatorStack) > 0 && end($operatorStack) != "(")
                {
                    $output = $output . array_pop($operatorStack);
                    //echo $output . "\n";
                }

                if (count($operatorStack) == 0)
                    throw new SyntaxException("missing a left parenthesis");

                array_pop($operatorStack); // lose the ( character
            }
            else if (!ctype_space($c)) // At this point, anything but a space
                throw new SyntaxException("Bad char: $c"); // is a syntax error
        }

        // Now we have gone through the whole string. It remains to put what is left
        // of the operator stack onto the final output.

        while(count($operatorStack) > 0)
        {
            $nextOperator = array_pop($operatorStack);
            if ($nextOperator == "(")
                throw new SyntaxException("missing a right parenthesis");
            else
                $output = $output . $nextOperator;
        }

        if($debug) echo "output of dsy($infix) is $output";
        return $output;
    }

    public static function isOperand($c)
    {
        return strlen($c) == 1 && ctype_alpha($c) || $c === "0" || $c === "1";
    }

    public static function isOperator($c)
    {
        return $c === "*" || $c === "+" || $c === "'";
    }

/**
 * The final step in parsing strings to Formulae. From a fairly standard algorithm
 * for parsing arithmetical postfix expressions, taken from google; I adapted it for
 * Boolean algebra,
 * @param  string $myPostfix A string in postfix notation
 * @return Formula           A Formula object
 */
    public static function postfixFormula( $myPostfix )
    {
        $debug = false;

        if (!is_string($myPostfix)) throw new Exception("postfixFormula needs a string");
        $stack = array();
        if($debug) echo "myPostfix is $myPostfix\n";

        for ($i = 0; $i < strlen($myPostfix); $i++)
        {
            $c = $myPostfix[$i];
            if($debug) echo "now considering $c\n";
            if (BoolParser::isOperand($c))
            {
                if ($c === "1" || $c === "0") // need to be careful with "0"s in php
                    array_push($stack, new Constant($c));
                else
                {
                    if($debug) echo( "pushing $c to stack\n");
                    array_push($stack, new Atom($c));
                }
            }
            else if ($c == "+")
            {
                $B = array_pop($stack);
                $A = array_pop($stack);
                array_push($stack, new Disjunction($A, $B));
            }
            else if ($c == "*")
            {
                $B = array_pop($stack);
                $A = array_pop($stack);
                array_push($stack, new Conjunction($A, $B));
            }
            else if ($c == "'")
            {
                $A = array_pop($stack);
                array_push($stack, new Negation($A));
            }
            else
                throw new SyntaxException("Bad char: $c");
        }

        $outputFormula = array_pop($stack);

        if (count($stack) > 0)
        {
            $s = "";
            while(count($stack) > 0)
                $s = $s . array_pop($stack)->toString();
            $s = strrev($s);
            throw new SyntaxException("Left over after parsing: $s");
        }

        return $outputFormula;
    }

/**
 * I needed this because php doesn't have EmptyStackExceptions. If you are using a
 * php array as a stack and you pop it when it's empty, it returns NULL.
 * @param  [type]  $a [description]
 * @param  boolean $b [description]
 * @return [type]     [description]
 */
    public static function checkEmptyStack($a, $b = "not null")
    {
        echo "a is " . gettype($a) . ", b is " . gettype($b) . "\n";
        if (is_null($a) || is_null($b))
            throw new SyntaxException("missing an operand");
    }

    public static function checkEmptyStackMono($a)
    {
        echo "a is " . $a . "\n";
        if (is_null($a))
            throw new SyntaxException("missing an operand");
    }
}