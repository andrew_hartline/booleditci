<?php

/**
 * A Disjunction is a BinaryFormula whose connective is +, read as logical 'or'.
 */
class Disjunction extends BinaryFormula
{
    public function __construct(Formula $l, Formula $r)
    {
        parent::__construct($l, $r);
        $this->myConnective = "+";
    }
}

?>