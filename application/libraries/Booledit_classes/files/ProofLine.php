<?php

require_once("includes.php");

/**
 * A Proof has zero or more of these things. Each ProofLine consists of a
 * formula and a justification, along with a little flag that BoolEdit can
 * adjust to indicate whether the line evaluates as acceptable according
 * to the rules for extending a proof.
 */
class ProofLine
{
    private $myFormula;
    private $myRule;
    private $ok;

/**
 * Class constructor
 * @param Formula $f the line's Formula
 * @param [type]  $r the line's EquivalenceRule (may be null)
 */
    public function __construct(Formula $f=null, EquivalenceRule $r = null)
    {
        if ($f == null) throw new SyntaxException('no formula passed to constructor');
        $this->myFormula = $f;
        $this->myRule = $r;
    }

    public function isOk()
    {
        return $this->ok;
    }

    public function setOk($bool)
    {
        if (!is_bool($bool))
            throw new IllegalArgumentException("setOk wants a boolean");
        else
            $this->ok = $bool;
    }

    public function getFormula()
    {
        return $this->myFormula;
    }

    public function getRule()
    {
        return $this->myRule;
    }

    public function toString()
    {
        $rule = ($this->myRule == null)? "" : explode(" ", $this->myRule->toString())[0];
        return $this->myFormula->toString() . " : " . $rule;
    }
}

?>