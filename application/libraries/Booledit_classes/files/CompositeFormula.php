<?php

require_once("includes.php");

/**
 * A CompositeFormula has one or more subFormulae.
 */
abstract class CompositeFormula extends Formula
{
    /**
     * Recursive function for uniform substitution of a formula for an
     * atom. Travels down the branches of my tree until it finds an
     * atom / leaf, then subs in Formula $f for that leaf if it == $a.
     * @param  Formula $f Formula that is to be subbed for all occurrences of $a
     * @param  Atom    $a Atom that is going to be swapped for $f every time it
     *                    occurs
     * @return Formula    Formula obtained from input by uniform substutution of $f
     *                    for $a.
     */
    public function uSub(Formula $f, Atom $a)
    {
        $output = $this;
        for ($i = 0; $i < $this->getArity(); $i++)
            $this->myFormulae[$i] = $this->myFormulae[$i]->uSub($f, $a);

        return $output;
    }

    /**
     * Answers the question: Are you derivable from me via uniform substitution?
     * @param  Formula $subbedFormula [description]
     * @return [type]                 [description]
     */
    public function uSubbable(Formula $subbedFormula)
    {
        $subMap = array();

        return $this->uSubbableConstrained($subbedFormula, $subMap);
    }

    /**
     * Answers the question: Are you derivable from me via uniform substitution,
     * if we assume that every substitution in this table must be made?
     * @param  Formula $otherFormula [description]
     * @param  [type]  $subMap       [description]
     * @return [type]                [description]
     */
    public function uSubbableConstrained(Formula $otherFormula,
                                         &$subMap)
    {
        $output = false;

        if ($this->myConnective == $otherFormula->getConnective())
        {
            $output = true;

            for ($i = 0; $i < $this->getArity(); $i++)
            {
                $otherFormulae = $otherFormula->getFormulae();
                if (!$this->myFormulae[$i]->uSubbableConstrained($otherFormulae[$i],
                                                                  $subMap))
                    $output = false;
            }
        }

        return $output;
    }
}