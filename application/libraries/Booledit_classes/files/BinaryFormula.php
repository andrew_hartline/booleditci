<?php

require_once("includes.php");

/**
 * A BinaryFormula is composed of a connective plus two subFormulae.
 */
class BinaryFormula extends CompositeFormula
{
    public function __construct(Formula $l, Formula $r)
    {
        if ($l instanceof Formula && $r instanceof Formula)
            $this->myFormulae = array($l, $r);
        else throw new SyntaxException("I need fully constructed Formulae");
    }

    public function toString()
    {
        if ($this->myFormulae[0] instanceof SimplePrint)
            $output = $this->myFormulae[0]->toString();
        else
            $output = "(" . $this->myFormulae[0]->toString() . ")";

        $output = $output . $this->myConnective;

        if ($this->myFormulae[1] instanceof SimplePrint)
            $output = $output . $this->myFormulae[1]->toString();
        else
            $output = $output . "(" . $this->myFormulae[1]->toString() . ")";

        return $output;
    }
}

?>