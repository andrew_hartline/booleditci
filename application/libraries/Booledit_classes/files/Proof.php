<?php
require_once("includes.php");

class Proof extends LinkedLines
{
    private $myEquivalence;

    public function __construct($l, $r)
    {
        parent::__construct();
        $this->myEquivalence = new Equivalence($l, $r);
    }

    public function getEquivalence()
    {
        return $this->myEquivalence;
    }



/**
 * This method allows the Proof to evaluate each of its lines for OK-ness
 * according to its Rules.
 *
 * The first line is OK iff it is an instance of either side of the Proof's
 * Equivalence.
 *
 * Every line after that is OK iff it and its immediate predecessor are a
 * substitution instance of the pair of Formulae specified in the
 * EquivalenceRule cited at that line.
 * @return boolean True iff every line is ok (includes situation where there
 *                 aren't any lines).
 */
    public function evaluateLines()
    {
        $allGood = true;

        foreach ($this->myLines as $lineNum => $line)
        {
            $f = $line->getFormula();
            $fstr = $f->toString();
            if ($lineNum == 0)
            {
                if ( $f->equals($this->myEquivalence->getLeft()) ||
                     $f->equals($this->myEquivalence->getRight()) )
                {
                    $line->setOk(true);
                }
                else
                    $line->setOk(false);
            }
            else
            {
                $rule = $line->getRule();
                $previousLine = $this->myLines[$lineNum-1]->getFormula();
                if ($rule != null)
                    $ok = $rule->licensedTransitionPart($previousLine,
                                                        $f);
                else $ok = false;
                $line->setOk($ok);
                if (!$ok)
                    $allGood = false;
            }
        }

        return $allGood;
    }

/**
 * This method allows the Proof to determine whether it's complete.
 *
 * The Proof is done iff its first and last ProofLines represent the two sides
 * of its Equivalence, and each of its lines evaluates as ok.
 * @return boolean Whether the Proof is done.
 */
    public function isDone()
    {
        $output = true;
        $size = count($this->myLines);

        if ($size == 0)
            $output = false;
        else
        {
            $first = $this->myLines[0]->getFormula();
            $last = $this->myLines[$size - 1]->getFormula();

            $output = ( $first->equals($this->myEquivalence->getLeft()) &&
                        $last->equals($this->myEquivalence->getRight()) )
                     ||
                      ( $last->equals($this->myEquivalence->getLeft()) &&
                        $first->equals($this->myEquivalence->getRight()) );
        }

        if ($output)
            $output = $this->evaluateLines();

        return $output;
    }

/**
 * Makes this proof into XML. We can use this to save the Proof to a db.
 * Also handily encapsulates a lot of info for the controller to pass to
 * a view.
 * @return string xml
 */
    public function serialize()
    {
        $in_complete = ($this->isDone())? "complete" : "incomplete";
        // this line calls a function that updates all evaluations

        $myXML = "<proof>";
        $myXML .= "<equivalence>" . $this->myEquivalence->toString();
        $myXML .= "</equivalence>";
        $myXML .= "<numLines>" . $this->numLines . "</numLines>";
        $myXML .= "<solved>$in_complete</solved>";
        $myXML .= "<lines>";

        foreach ($this->myLines as $line)
        {
            $formula = $line->getFormula()->toString();
            $rule = ($line->getRule() == null)?
                        "null" : $line->getRule()->getName();
            $ok = ($line->isOk())? "ok": "x";
            $myXML .= "<line><formula>$formula</formula>";
            $myXML .= "<rule>$rule</rule><ok>$ok</ok></line>";
        }

        $myXML .= "</lines>";
        $myXML .= "</proof>";

        return $myXML;
    }

    public function showEquivalence()
    {
        echo $this->myEquivalence->toString();
    }

    /**
     * Unbakes the xml made by the function $this->serialize().
     * @param  string $string Some xml
     * @return Proof          A Proof object
     */
    public static function unSerialize($string)
    {
        $x = simplexml_load_string($string);
        $array = explode("=", $x->equivalence);
        $p = new Proof($array[0], $array[1]);

        foreach($x->lines->line as $line)
        {
            $p->addLine((string)$line->formula, (string)$line->rule);
        }

        return $p;
    }
}

?>