<?php

require_once("includes.php");

/**
 * An interface that exists to prevent formulae without complex inner structure
 * from displaying outer parentheses. Used in Formula::toString() method.
 */
interface SimplePrint {}

?>
