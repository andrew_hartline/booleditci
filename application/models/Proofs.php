<?php

/**
 * Single model for both Proofs and SSimplifications. Since a simplification is
 * just a proof without a clear object, I've elected tp just have it be a Proof
 * whose object is a definite formula that the user doesn't know, and whose c
 * completion status is never available to the user. This makes programming and
 * maintenance a lot simpler.
 */

/** I have chosen to give creation and update funcitonality to this model, with
 > read and delete functionality going to User. This is because a Proof has all
 * the information it needs for those functions, but has no knowledge of the
 * other data on its table. I hope that this is sound enough OO reasoning.
 */
class Proofs extends CI_Model
{
    /*
    var $data = array(
        array('id' => '1', 'ownerID' => '1', 'type' => 'p', 'item' => "p*p' = q", 'numLines' => '15', 'in_complete' => 'complete'),
        array('id' => '11', 'ownerID' => '1', 'type' => 's', 'item' => "p*p''''''''''''", 'numLines' => '15000', 'in_complete' => 'complete'),
    );*/

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // retrieve a single proof
    public function get($which)
    {
        $query = $this->db->get_where(PROOFS, array('id' => $which));

        // iterate over the data until we find the one we want
        if ($query->num_rows() > 0)
            return $query->row();

        return null;
    }

    // retrieve all proofs belonging to one user
    public function getUserProofs($who)
    {
        $query = $this->db->get_where(PROOFS, array('ownerID' => $who));

        return $query->result_array();
    }

    // retrieve all of the quotes
    public function all()
    {
        return $this->data;
    }

    public function insert($userId, $type, $item)
    {
        $data = $this->gatherData($userId, $type, $item);

        $this->db->trans_start();  // Ensure mutual exclusion ...
        $this->db->insert(PROOFS, $data);
        $insertId = $this->db->insert_id(); // ... so that this return is correct
        $this->db->trans_complete();

        return $insertId;
    }

    public function delete($userId, $id)
    {
        $query = $this->db->get_where(PROOFS, array('id' => $id));
        echo "id is $id";
        $this->db->delete(PROOFS, array("id" => $id));
    }

    public function update($id, $userID, $type, $item)
    {
        $data = $this->gatherData($userID, $type, $item);

        $this->db->where('id', $id);
        $this->db->update(PROOFS, $data);
    }

    private function gatherData($userID, $type, $item)
    {
        $proof = Proof::unserialize($item);
        $numLines = $proof->getNumLines();
        $complete = $proof->isDone();

        $data = array
        (
            'ownerID'  => $userID,
            'type'     => $type,
            'item'     => $item,
            'numLines' => $numLines,
            'complete' => $complete
        );

        return $data;
    }
}