<?php



class Users extends CI_Model
{
    /*
    var $data = array(
        array('id' => '1', 'numProofs' => '1', 'email' => 'a@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/21/2015'),
        array('id' => '2', 'numProofs' => '100', 'email' => 'b@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/20/2015'),
        array('id' => '3', 'numProofs' => '12', 'email' => 'c@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/22/2015'),
        array('id' => '4', 'numProofs' => '0', 'email' => 'd@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/25/2015'),
        array('id' => '5', 'numProofs' => 'foo', 'email' => 'e@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/1/2015'),
        array('id' => '8', 'numProofs' => '34', 'email' => 'f@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/24/2015'),
        array('id' => '6', 'numProofs' => '123', 'email' => 'g@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/21/2014'),
        array('id' => '10', 'numProofs' => '-50', 'email' => 'h@email.com', 'password' => 'somehashedstuff', 'lastLogin' => '1/21/2013')
    ); */

    // Constructor
    public function __construct()
    {
        parent::__construct();
    }

    // Counts the proofs belonging to a User as specified in a result array;
    private function countProofs($row)
    {
        $userId = $row['id'];
        $query = $this->db->get_where(PROOFS, array('ownerId' => $userId));

        return $query->num_rows();
    }

    // retrieve a single user
    public function get($who)
    {
        $query = $this->db->get_where(USERS, array('id' => $who));
        $row = $query->row_array();
        $row['numProofs'] = $this->countProofs($row);

        return $row;
    }

    // retrieve all of the users
    public function all()
    {
        $query = $this->db->query("SELECT * FROM " . USERS);
        $array = $query->result_array();

        foreach($array as &$row)
            $row['numProofs'] = $this->countProofs($row);

        return $array;
    }

}