<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>BoolEdit | {title}</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap-select.js"></script>
        <script src="/js/bootstrap-select.js">$('.selectpicker').selectpicker();</script>
    </head>
    <body>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="/" class="navbar-brand">
                        <img src="/img/booledit.png" height="30px" style="margin-top: -5px;" alt="BoolEdit">
                    </a>
                </div>
                <a href="/load" class="btn btn-primary btn-warning">Load</a>
                <a href="/prove/new" class="btn btn-primary btn-danger">New proof</a>
                <a href="/simplify/new" class="btn btn-primary ">New simplification</a>
                <a href="/logout" class="btn btn-primary btn-danger" style="float: right">Logout</a>
                <a href="/admin" class="btn btn-primary" style="float: right">Admin</a>
            </div>
        </nav>
        <div class="container">
            {content}
        </div>
    </body>
</html>
