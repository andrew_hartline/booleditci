<h3 id="equiveval">Simplification of {item}</h3>
<form method="POST" action="handleInput" accept-charset="UTF-8">
    <table id="lines" class="table table-border table-striped">
        <tr><th></th><th>entry</th><th>rule</th><th>evaluation</th><th></th></tr>
        {lines}
            <tr>
                <td>({lineNum})</td><td>{formula}</td><td>{rule}</td><td>{ok}</td>
                <td><input type="checkbox" name = "delete[]" value="{lineNum}">
                    <label for = "delete[]">Delete</label>
                </td>
            </tr>
        {/lines}
    </table>
    <hr>
    <div class="control-group error">
        <label for="proofline">{prompt}</label>
        <input class="form-control" name="proofline" type="text" id="proofline">
        <span class="help-inline">{errormessage}</span><br>
    </div>
    <label for="rule" {hidden}>Enter rule:</label>
    <br>
    <select name="rule"  {hidden}>
        <option value = "null"></option>
    {rules}
        <option value = "{name}">{name}  {equivalence}</option>
    {/rules}
    </select>
    <hr>
    <input class="btn btn-primary btn-success" name = "update" type="submit" value="Update"/>
    <input class="btn btn-primary" name = "save" type = "submit" value = "Save"/>
</form>
