
<div class="container">
    <h1>Welcome {email}</h1>
    <h3>Select a proof or simplification</h3>
    <ul class="list-group">
        {userproofs}
        <div>
            <a href='{controller}/{id}' class='list-group-item'>
                {proof_simplification} of {item} ({numLines} line{s}{in_complete})
            </a>
            <a href="/User/delete/{id}">Delete</a>
        </div>
        {/userproofs}
    </ul>
</div>
