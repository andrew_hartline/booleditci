<h1>User {email}</h1>

<h3>User Proofs (count = {numProofs})</h3>
<ul class="list-group">
    {proofs}
        <a href='/admin/seeProof/{id}' class='list-group-item'>
            {proof_simplification} of {item} ({numLines} line{s}{in_complete})
        </a>
    {/proofs}
</ul>
