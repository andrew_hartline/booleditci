<div class="col-md-3">
    <form method="POST" action="start" accept-charset="UTF-8">
        <label for="equivalence">Enter equivalence</label>
        <input name="equivalence" type="text" id="equivalence">
        <ul>
        {problems}
            <li class="help-inline" for="equivalence">{problem}</li>
        {/problems}
        </ul>
        <br>

        <input class="btn btn-primary btn-success" type="submit" value="Submit">
    </form>
     <hr>
</div>
