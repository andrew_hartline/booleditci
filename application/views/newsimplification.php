<div class="col-md-3">
    <form method="POST" action="start" accept-charset="UTF-8">
        <label for="complex">Enter the formula you want to simplify</label>
        <input name="complex" type="text" id="complex">
        <ul>
        {problems}
            <li class="help-inline" for="complex">{problem}</li>
        {/problems}
        </ul>
        <br>
        <input class="btn btn-primary btn-success" type="submit" value="Submit">
    </form>
     <hr>
</div>
