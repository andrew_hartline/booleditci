<h3 id="equiveval">{proof_simplification} of {item} {in_complete}</h3>
    Author: {email} <br>
    <table id="lines" class="table table-border table-striped">
        <tr><th></th><th>entry</th><th>rule</th><th>evaluation</th><th></th></tr>
        {lines}
            <tr><td>({lineNum})</td><td>{formula}</td><td>{rule}</td><td>{ok}</td>
                <td>
                    <input type="checkbox" name = "delete[]" value="{lineNum}">
                    <label for = "delete[]">Delete</label>
                </td>
            </tr>
        {/lines}
    </table>
    <hr>
    <a href="/admin/seeUser/{ownerId}" class="btn btn-primary btn-warning">Return</a>