
<div class="container">
    <h1>BoolEdit Users</h1>
    <ul class="list-group">
        {users}
            <a href='admin/seeUser/{id}' class='list-group-item'>
                {email} ({numProofs} proof{s}, last login {lastLogin})
            </a>
        {/users}
    </ul>
</div>
