<?php

/**
 * core/MY_Controller.php
 *
 * Default application controller
 *
 * @author      JLP
 * @copyright           2010-2013, James L. Parry
 * ------------------------------------------------------------------------
 */
class Application extends CI_Controller {

    protected $data = array();      // parameters for view components
    protected $id;                  // identifier for our content

    /**
     * Constructor.
     * Establish view parameters & load common helpers
     */

    function __construct() {
        parent::__construct();
        $this->data = array();
        $this->data['title'] = 'BoolEdit';    // our default title
        $this->errors = array();
        $this->data['pageTitle'] = 'welcome';   // our default page
    }

    /**
     * Render this page
     */
    function render() {
        //$this->data['menubar'] = $this->parser->parse('_menubar', $this->config->item('menu_choices'),true);
        $this->data['content'] = $this->parser->parse($this->data['pagebody'],
                                                      $this->data, true);

        // finally, build the browser page!
        $this->data['data'] = &$this->data;
        $this->parser->parse('_template', $this->data);
    }
}

/**
 * Abstract controller class to hold common functions for Proofs and Simplifications.
 */
class BoolEditController extends Application
{
    protected $type; // char p for Proof, s for Simplification
    protected $updateView; // the html view

    // Proof and Simplification forms hav4 two buttons, one for each of Update and Save functions
    // This method decides what button was pressed and acts accordingly.
    public function handleInput()
    {
        $x = $this->updateProof();

        if (!is_null($x))
        {
            if(isset ($_POST['save']))
            {
                $this->save();
            }
            else
            {
                $this->update();
            }
        }
        else
        {
            $this->create();
        }
    }

    private function makeAssocLinesArray(SimpleXmlElement $x)
    {
        $lines = array();
        $lineNum = 1;
        foreach ($x->lines->line as $line)
            $lines[] = array_merge(array('lineNum' => $lineNum++),
                                   (array)$line);

        return $lines;
    }

    // Update the proof or simplification in the current session.
    public function update()
    {
        $this->data['errormessage'] = "";
        $xml = $this->session->userdata('proof');

        if (!is_null($xml))
        {
            $x = simplexml_load_string($xml);
            $this->data['pagebody'] = $this->updateView;
            $this->data['equivalence'] = $x->equivalence; // for proofs
            $this->data['item'] = explode('=', $x->equivalence)[0];
                // for simplifications
            $this->data['in_complete'] = $x->solved;
                // will be ignored if you have a simplification

            $lines = $this->makeAssocLinesArray($x);

            if ($x->numLines > 0)
            {
                $this->data['lines'] = $lines;
                $this->data['prompt'] = "Enter line:";
                $this->data['rules'] = BooleRules::ToArray();
                $this->data['hidden'] = '';
            }
            else
            {
                $this->data['lines'] = array();
                $this->data['prompt'] = "Enter first line:";
                $this->data['hidden'] = 'hidden';
            }

            $this->render();
        }
        else
            $this->create();
    }

    // Update the current session proof if there's something in input.
    // Returns xml of the proof on success, null on failure.
    protected function updateProof()
    {
        if ($this->session->userdata('proof'))
        {
            $proof = Proof::unserialize($this->session->userdata('proof'));

            // Handle deletions
            if (isset($_POST['delete']))
            {
                foreach ($_POST['delete'] as $num)
                    $proof->deleteLine($num);

                $proof->resetLineNumbering();  // fill in all the gaps we just made
            }

            if (isset($_POST['proofline']) && trim($_POST['proofline']) !== "")
            {
                $proofLineString = strip_tags($_POST['proofline']);
                $ruleString = (isset($_POST['rule']))? $_POST['rule'] : "null";
                try
                {
                    $proof->addLine($proofLineString, $ruleString);
                }
                catch (SyntaxException $e)
                {
                    $this->data['errormessage'] = "Bad syntax";
                }
            }

            $proof->evaluateLines();
            $proofXML = $proof->serialize();

            $this->session->set_userdata('proof', $proofXML);

            return $proofXML;
        }
        else
        {
            return null; // if you get here, something is bad
        }
    }

    // A user can begin a proof/ simplification without saving it. This function checks
    // whether the  user has saved their proof; if so, it directs the model to
    // update the  entrr for that proof. Otherwise it gets the model to insert
    // a  new entry and keeps track of the id for that entry
    protected function save()
    {
        $bogusUserId = 1; // maybe we'll fix this in v3

        if ($this->session->userdata('proof'))
        {
            if ($this->session->userdata('proofId') &&
                $this->session->userdata('proofId') > 0) // i.e. if user has saved already
            {
                // public function update($id, $userID, $type, $item)
                $this->Proofs->update($this->session->userdata('proofId'),
                                      $bogusUserId,
                                      $this->type, // p for Proof, s for simplification
                                      $this->session->userdata['proof']);
            }
            else   //  public function insert($userID, $type, $item)
            {
                $id = $this->Proofs->insert($bogusUserId,
                                            $this->type, // p for Proof, s for Simplification
                                            $this->session->userdata['proof']);

                $this->session->set_userdata('proofId', $id);
            }
        }

        $this->update();
    }

    // retrieve a proof from the db
    public function get($i)
    {
        $proof = $this->Proofs->get($i);
        $this->session->userdata['proof'] = $proof->item;
        $this->session->userdata['proofId'] = $proof->id;

        $this->update();
    }

     /**
     * Make a new Proof object based on submitted equivalence string.
     * Semd errors to Session for display to the user.
     * @param  [type] $equivalence String equivalence
     * @return Proof               Proof object or null
     */
    protected function constructProof($equivalence)
    {
        $this->session->set_userdata('proofId', -1);
            // this will keep track of whether user has saved
        $problems = array(); // will be empty at the end if all is well
        $output = null; // will be something at the end if all is well

        $array = explode("=", $equivalence); // already stripped tags in start()

        if (count($array) != 2) // This logic creates some bogus error messages when the user is trying to simplify. TODO fix in 3.0
        {
            $problems[] = array('problem' => "Entry needs to be in the form A=B");
        }
        else  // If we have something in the form A = B, evaluate A and B
        {     // separately and collect data about whatever syntax problems exist.
            $l = strip_tags($array[0]);
            $r = strip_tags($array[1]);

            try
            {
                $A = BoolParser::formulaFactory($l);
            }
            catch (SyntaxException $e)
            {
                $problems[] = array('problem' => "Bad syntax in $l: " . $e->getMessage());
            }

            try
            {
                $B = BoolParser::formulaFactory($r);
            }
            catch (SyntaxException $e)
            {
                $problems[] = array('problem' => "Bad syntax in $r: " . $e->getMessage());
            }

            if (count($problems) === 0)
                $output = new Proof($l, $r);
        }

        // This is supposed to blow out any old problems that exist.
        $this->session->set_userdata('newProofProblems', $problems);

        return $output;
    }
}



/* End of file MY_Controller.php */
/* Location: application/core/MY_Controller.php */