<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller lets users write simplifications.
 */
class Simplify extends BoolEditController
{

    public function __construct()
    {
        parent::__construct();
        $this->type = 's';
        $this->updateView = "editsimplification";
    }

    public function index($i)
    {
        // require_once ("application/third_party/booledit/includes.php");

        // require_once('C:\wamp\www\booledit\third_party\booledit\Conjuction.php');

        $this->create();
    }

    public function create()
    {
        if ($this->session->userdata('newProofProblems'))
            $this->data['problems'] = $this->session->userdata('newProofProblems');
        else $this->data['problems'] = array();

        $this->data['pagebody'] = 'newsimplification';
        $this->render();
    }

    // Get a new Simplification made from the user-entered formula.
    public function start()
    {
        $equivalence = "";
        $unlikely = "u+u+d+d+l+r+l+r+a+b+b+a+s";
            // Part of a behind the scenes hack. I don't want the user to enter
            // this as the first line because it will evaluate as correct when
            // in fact it is not. Therefore it is an unlikely sequence.

        if (isset($_POST['complex']))
            $equivalence = strip_tags($_POST['complex']) . "= $unlikely";
            // Behind the scenes the Simplification is a Proof of a bogus equivalence.
            // Don't tell my users.

        $proof = $this->constructProof($equivalence);

        if (!is_null($proof))
        {
            $this->session->set_userdata('proof', $proof->serialize());
            $this->update();
        }
        else
        {
            $this->create();
        }
    }
}