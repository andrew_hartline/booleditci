<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * This controller is responsible for showing the individual user their proofs.
 */
class User extends Application {

    public function __construct()
    {
        parent::__construct();
    }

    public function index($uId = 1) // we'll say user id=1 is always logged in for now
    {
        //require_once ("application/third_party/booledit/includes.php");
        $userProofs = $this->Proofs->getUserProofs($uId);

        // Let's modify the array a bit for viewing ... need to pass by ref
        foreach($userProofs as &$proof)
        {
            //var_dump($proof);
            $x = simplexml_load_string($proof['item']);

            $proof['item'] = ($proof['type'] === 'p')?
                                 $x->equivalence :
                                 explode('=', $x->equivalence)[0];
                // Explanation of this hack can be found elsewhere in the
                // comments: basically I am treating simplifications as
                // Proofs whose Equivalence's LHS is the formula that the
                // user knows, and whose RHS is some random syntactically
                // correct garbage that they are not allowed to see

            $id = $proof['id'];

            $completeness = ($proof['complete'])? "complete" : "incomplete";
                // I hate to do this but it seemed dumber to make an html partial just for that single word
            $proof['proof_simplification'] = ($proof['type'] == 'p')?
                                                 'Proof' : 'Simplification';
            $proof['controller'] = ($proof['type'] == 'p')?
                                       'prove' : 'simplify';
            $proof['s'] = ($proof['numLines'] == 1)? '' : 's';
            $proof['in_complete'] = ($proof['type'] === 'p')?
                                       ', ' . $completeness : '';

        }  // You can't complete a simplification since, like, what is simplicity?

        $this->data['userproofs'] = $userProofs;

        $this->data['email'] = $this->Users->get(1)['email']; // we only have one user
        $this->data['title'] = 'Proofs';
        $this->data['pagebody'] = 'crudgrid';
        $this->render();
    }

    public function delete($id)
    {
        $this->Proofs->delete(1, $id); // 1 represents uid of current user
        // todo: get this working
        $this->index();
    }

}