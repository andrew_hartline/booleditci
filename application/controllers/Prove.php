<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prove extends BoolEditController
{
    public function __construct()
    {
        parent::__construct();
        $this->type = 'p';
        $this->updateView = "editproof";
    }

    public function index()
    {
        $this->start();
    }

    // Get an equivalence from the user to start a new proof.
    public function create()
    {
        if ($this->session->userdata('newProofProblems'))
            $this->data['problems'] = $this->session->userdata('newProofProblems');
        else $this->data['problems'] = array();

        $this->data['pagebody'] = 'newproof';
        $this->render();
    }

    // Get a new Proof made from the user-entered equivalence.
    public function start()
    {
        $equivalence = "";

        if (isset($_POST['equivalence']))
            $equivalence = strip_tags($_POST['equivalence']);

        $proof = $this->constructProof($equivalence);

        if (!is_null($proof))
        {
            $this->session->set_userdata('proof', $proof->serialize());
            $this->update();
        }
        else
        {
            $this->create();
        }
    }
}
