<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Application
{
    /**
     * Show drillable table of all users.
     * @return [type] [description]
     */
    public function index()
    {
        $allUsers = $this->Users->all();

        foreach ($allUsers as &$user)
        {  // set pluralization
            $user['s'] = ($user['numProofs'] == 1)? '' : 's';
        }

        $this->data['title'] = 'Admin Page';
        $this->data['users'] = $allUsers;
        $this->data['pagebody'] = 'admingrid';

        $this->render();
    }

    /**
     * View user data, including a table of all proofs belonging to that user.
     * @param  [type] $id user ID number
     * @return [type]     [description]
     */
    public function seeUser($id)
    {
        $this->data = array_merge($this->data, $this->Users->get($id));
        $userProofs = $this->Proofs->getUserProofs($id);

        // Let's modify the array a bit for viewing
        foreach($userProofs as &$proof)
        {
            $xml = $proof['item'];
            $x = simplexml_load_string($xml);

            if ($proof['type'] == 'p')
            {
                $proof['proof_simplification'] = 'Proof';
                $proof['item'] = $x->equivalence;
            }
            else // if you have a simplification
            {
                $proof['proof_simplification'] = 'Simplification';
                $proof['item'] = explode('=', $x->equivalence)[0];
            }

            $completeness = ($proof['complete'])? ", complete" : ", incomplete";
                // will both be ignored if you have a simplification

            $proof['s'] = ($proof['numLines'] == 1)? '' : 's'; // pluralization
            $proof['in_complete'] = ($proof['type'] === 'p')?
                                        $completeness : '';
        }  // You can't complete a simplification since, like, what is simplicity?

        $this->data['proofs'] = $userProofs;
        $this->data['pagebody'] = 'oneuser';

        $this->render();
    }

    /**
     * View a particular proof or simplification (cannot edit it)
     * @param  [type] $id Db id of the proof
     * @return [type]     [description]
     */
    public function seeProof($id)
    {
        $proof = (array)$this->Proofs->get($id);
        $this->data = array_merge($this->data,
                                  $proof);

        $completeness = ($proof['complete'])? "complete" : "incomplete";
        $xml = $proof['item'];   // todo: refactor this logic into its own function for 3.0
        $x = simplexml_load_string($xml); // I'm seeing too much of it in 2.0

        if ($proof['type'] == 'p')
        {
            $this->data['proof_simplification'] = 'Proof';
            $this->data['item'] = $x->equivalence;
            $this->data['in_complete'] = ' (' . $completeness . ")";
        }
        else // if you have a simplification
        {
            $this->data['proof_simplification'] = 'Simplification';
            $this->data['item'] = explode('=', $x->equivalence)[0];
            $this->data['in_complete'] = "";
        }

        if ($x->numLines > 0)
            $this->data['lines'] = $this->makeAssocLinesArray($x);
        else
            $this->data['lines'] = array();

        $this->data['s'] = ($proof['numLines'] == 1)? '' : 's'; //plural
        $user = $this->Users->get($proof['ownerId']);
        $this->data['email'] = $user['email'];
        $this->data['pagebody'] = 'viewproof';
        $this->render();
    }

    private function makeAssocLinesArray(SimpleXmlElement $x)
    {
        $lines = array();
        $lineNum = 1;

        foreach ($x->lines->line as $line)
            $lines[] = array_merge(array('lineNum' => $lineNum++),
                                   (array)$line);

        return $lines;
    }
}

