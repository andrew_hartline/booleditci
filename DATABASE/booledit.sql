-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2015 at 07:48 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `booledit`
--

-- --------------------------------------------------------

--
-- Table structure for table `proofs`
--

CREATE TABLE IF NOT EXISTS `proofs` (
`id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `type` char(1) NOT NULL,
  `numLines` int(11) NOT NULL,
  `complete` tinyint(1) NOT NULL,
  `item` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proofs`
--

INSERT INTO `proofs` (`id`, `ownerId`, `type`, `numLines`, `complete`, `item`) VALUES
(6, 1, 'p', 0, 0, '<proof><equivalence>p = (p*o)*o</equivalence><numLines>0</numLines><solved>incomplete</solved><lines></lines></proof>'),
(7, 1, 's', 1, 0, '<proof><equivalence>((((p+p)+p)+p)+p)+p = (((((((((((u+u)+d)+d)+l)+r)+l)+r)+a)+b)+b)+a)+s</equivalence><numLines>1</numLines><solved>incomplete</solved><lines><line><formula>p</formula><rule>null</rule><ok>x</ok></line></lines></proof>'),
(8, 1, 'p', 1, 0, '<proof><equivalence>p = q</equivalence><numLines>1</numLines><solved>incomplete</solved><lines><line><formula>p</formula><rule>null</rule><ok>ok</ok></line></lines></proof>'),
(9, 1, 'p', 0, 0, '<proof><equivalence>p = q</equivalence><numLines>0</numLines><solved>incomplete</solved><lines></lines></proof>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` text,
  `lastLogin` date NOT NULL DEFAULT '1970-01-01'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `lastLogin`) VALUES
(1, 'exampleStudent@email.com', 'fc5e038d38a57032085441e7fe7010b0', '1970-01-01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `proofs`
--
ALTER TABLE `proofs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `proofs`
--
ALTER TABLE `proofs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
